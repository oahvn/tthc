package com.haonguyen.tutorial;

public abstract class ThueBao {
	protected String name;
	protected String address;
	protected String phonenumber;
	protected int mb;

	protected ThueBao() {
		super();
	}

	protected ThueBao(String name, String address, String phonenumber, int mb) {
		this.name = name;
		this.address = address;
		this.phonenumber = phonenumber;
		this.mb = mb;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public int getMb() {
		return mb;
	}

	public void setMb(int mb) {
		this.mb = mb;
	}

	@Override
	public String toString() {
		return "ThueBao [name=" + name + ", address=" + address + ", phonenumber=" + phonenumber + ", mb=" + mb + "]";
	}

	public abstract double fee();

}
