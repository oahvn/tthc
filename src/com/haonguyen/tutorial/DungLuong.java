package com.haonguyen.tutorial;

public class DungLuong extends ThueBao {

	private static final double DEFAULT_FEE = 30000;

	public DungLuong() {
		super();
	}

	public DungLuong(String name, String address, String phonenumber, int megaBytes) {
		super(name, address, phonenumber, megaBytes);
	}

	@Override
	public double fee() {
		return DEFAULT_FEE + ((this.mb > 2000) ? (2000 * 55 + (this.mb - 2000) * 41) : (this.mb * 55));
	}

	@Override
	public String toString() {
		return "DungLuong [mb=" + mb + ", name=" + name + ", address=" + address + ", phonenumber=" + phonenumber
				+ ", cuoc=" + fee() + "]";
	}

}
