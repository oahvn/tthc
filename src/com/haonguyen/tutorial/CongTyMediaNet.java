package com.haonguyen.tutorial;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CongTyMediaNet {
	private List<ThueBao> list;

	@SuppressWarnings("resource")
	private ThueBao nhap(ThueBao thuebao) {
		System.out.print("\tHọ tên: ");
		thuebao.setName(new Scanner(System.in).nextLine());
		System.out.print("\tĐịa chỉ: ");
		thuebao.setAddress(new Scanner(System.in).nextLine());
		System.out.print("\tSố điện thoại: ");
		thuebao.setPhonenumber(new Scanner(System.in).nextLine());
		if (thuebao instanceof DungLuong) {
			System.out.print("\tSố dung lượng: ");
			thuebao.setMb(new Scanner(System.in).nextInt());
		}
		return thuebao;
	}

	@SuppressWarnings("resource")
	public void createListThueBao() {
		System.out.print("Nhập số lượng thuê bao: ");
		int n = new Scanner(System.in).nextInt();
		list = new ArrayList<ThueBao>(n);
		for (int i = 0; i < n; i++) {
			ThueBao thuebao = null;
			System.out.println("Nhập 1: Thuê bao trọn gói");
			System.out.println("Nhập 2: Thuê bao theo dung lượng");
			int key = new Scanner(System.in).nextInt();
			switch (key) {
			case 1:
				thuebao = new TronGoi();
				list.add(nhap(thuebao));
				break;
			case 2:
				thuebao = new DungLuong();
				list.add(nhap(thuebao));
			default:
				break;
			}
		}
	}

	public void show() {
		list.forEach(thuebao -> {
			System.out.println(thuebao.toString());
		});
	}

	public void topFee() {
		double max = 0;
		for (ThueBao thuebao : list) {
			if (thuebao.fee() > max) {
				max = thuebao.fee();
			}
		}
		for (ThueBao thuebao : list) {
			if (thuebao.fee() == max) {
				System.out.println(thuebao.toString());
			}
		}
		// System.out.println("a");
	}
}
