package com.haonguyen.tutorial;

public class TronGoi extends ThueBao {

	private static final double CUOC = 350000;

	public TronGoi() {
		super();
	}

	public TronGoi(String name, String address, String phonenumber) {
		super(name, address, phonenumber, -1);
	}

	@Override
	public double fee() {
		return CUOC;
	}

	@Override
	public String toString() {
		return "TronGoi [name=" + name + ", address=" + address + ", phonenumber=" + phonenumber + ", mb=" + mb +", cuoc="+fee()+"]";
	}

}
